// CodeGear C++Builder
// Copyright (c) 1995, 2009 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'Fileoperator.pas' rev: 21.00

#ifndef FileoperatorHPP
#define FileoperatorHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member functions
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <Sysinit.hpp>	// Pascal unit
#include <Windows.hpp>	// Pascal unit
#include <Classes.hpp>	// Pascal unit
#include <Forms.hpp>	// Pascal unit
#include <Controls.hpp>	// Pascal unit
#include <Shellapi.hpp>	// Pascal unit
#include <Baseutils.hpp>	// Pascal unit
#include <Sysutils.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Fileoperator
{
//-- type declarations -------------------------------------------------------
#pragma option push -b-
enum TFileOperation { foCopy, foDelete, foMove, foRename };
#pragma option pop

#pragma option push -b-
enum TFileOperationFlag { foAllowUndo, foConfirmMouse, foFilesOnly, foMultiDestFiles, foNoConfirmation, foNoConfirmMkDir, foRenameOnCollision, foSilent, foSimpleProgress };
#pragma option pop

typedef Set<TFileOperationFlag, foAllowUndo, foSimpleProgress>  TFileOperationFlags;

class DELPHICLASS TFileOperator;
class PASCALIMPLEMENTATION TFileOperator : public Classes::TComponent
{
	typedef Classes::TComponent inherited;
	
private:
	#pragma pack(push,1)
	_SHFILEOPSTRUCTW FData;
	#pragma pack(pop)
	Classes::TStringList* FFrom;
	Classes::TStringList* FTo;
	Classes::TStringList* FLastFrom;
	Classes::TStringList* FLastTo;
	TFileOperation FLastOperation;
	TFileOperationFlags fLastFlags;
	bool fCanUndo;
	System::UnicodeString FProgressTitle;
	Classes::TComponent* FOwner;
	void __fastcall SetOperation(TFileOperation Value);
	TFileOperation __fastcall GetOperation(void);
	bool __fastcall GetWantMappingHandle(void);
	void __fastcall SetWantMappingHandle(bool Value);
	void __fastcall SetFlags(TFileOperationFlags Value);
	TFileOperationFlags __fastcall GetFlags(void);
	bool __fastcall GetOperFlag(unsigned F);
	void __fastcall SetOperFlag(unsigned F, bool V);
	void __fastcall ReadData(Classes::TReader* Reader);
	void __fastcall WriteData(Classes::TWriter* Writer);
	void __fastcall SwapStringList(Classes::TStringList* &FromL, Classes::TStringList* &ToL);
	
protected:
	virtual void __fastcall DefineProperties(Classes::TFiler* Filer);
	
public:
	__property void * NameMappings = {read=FData.hNameMappings};
	__property BOOL OperationAborted = {read=FData.fAnyOperationsAborted, nodefault};
	__property Classes::TStringList* OperandFrom = {read=FFrom, write=FFrom};
	__property Classes::TStringList* OperandTo = {read=FTo, write=FTo};
	__property bool CanUndo = {read=fCanUndo, nodefault};
	__property TFileOperation LastOperation = {read=FLastOperation, nodefault};
	__property Classes::TStringList* LastOperandFrom = {read=FLastFrom};
	__property Classes::TStringList* LastOperandTo = {read=FLastTo};
	__fastcall virtual TFileOperator(Classes::TComponent* aOwner);
	__fastcall virtual ~TFileOperator(void);
	bool __fastcall Execute(void);
	bool __fastcall UndoExecute(void);
	void __fastcall ClearUndo(void);
	
__published:
	__property TFileOperation Operation = {read=GetOperation, write=SetOperation, stored=false, nodefault};
	__property TFileOperationFlags Flags = {read=GetFlags, write=SetFlags, stored=false, nodefault};
	__property System::UnicodeString ProgressTitle = {read=FProgressTitle, write=FProgressTitle};
	__property bool WantMappingHandle = {read=GetWantMappingHandle, write=SetWantMappingHandle, stored=false, nodefault};
};


//-- var, const, procedure ---------------------------------------------------
extern PACKAGE System::ResourceString _SFileOperation;
#define Fileoperator_SFileOperation System::LoadResourceString(&Fileoperator::_SFileOperation)
extern PACKAGE void __fastcall Register(void);

}	/* namespace Fileoperator */
using namespace Fileoperator;
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// FileoperatorHPP
