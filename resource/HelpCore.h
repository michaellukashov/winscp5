#ifndef HelpCoreH
#define HelpCoreH

#define HELP_UNKNOWN_KEY                       "ssh#verifying_the_host_key"
#define HELP_DIFFERENT_KEY                     HELP_UNKNOWN_KEY
#define HELP_RESUME_TRANSFER                   "resume"
#define HELP_APPEND_OR_RESUME                  "resume#alternate_usage"
#define HELP_PARTIAL_BIGGER_THAN_SOURCE        HELP_RESUME_TRANSFER
#define HELP_SYNCHRONIZE                       "task_synchronize_full"
#define HELP_VERIFY_CERTIFICATE                "ftps"
#define HELP_FTP_CANNOT_OPEN_ACTIVE_CONNECTION "message_cannot_open_active_connection"
#define HELP_MESSAGE_HOST_IS_NOT_COMMUNICATING "message_host_is_not_communicating"
#define HELP_ERRORMSG_TIMEOUT                  "message_timeout_detected"

#endif // HelpCoreH
