// CodeGear C++Builder
// Copyright (c) 1995, 2009 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'Filechanges.pas' rev: 21.00

#ifndef FilechangesHPP
#define FilechangesHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member functions
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <Sysinit.hpp>	// Pascal unit
#include <Sysutils.hpp>	// Pascal unit
#include <Windows.hpp>	// Pascal unit
#include <Classes.hpp>	// Pascal unit
#include <Compthread.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Filechanges
{
//-- type declarations -------------------------------------------------------
typedef void __fastcall (__closure *TFileDeleteEvent)(System::TObject* Sender, Classes::TStringList* Files);

class DELPHICLASS TFileDeleteThread;
class PASCALIMPLEMENTATION TFileDeleteThread : public Compthread::TCompThread
{
	typedef Compthread::TCompThread inherited;
	
private:
	TFileDeleteEvent fOnSignalDelete;
	Classes::TStringList* fFiles;
	Classes::TStringList* fDelFiles;
	_FILETIME fEndTime;
	
protected:
	virtual void __fastcall Execute(void);
	virtual void __fastcall DoTerminate(void);
	void __fastcall DoOnSignalDelete(void);
	
public:
	__fastcall TFileDeleteThread(Classes::TStringList* Files, unsigned TimeOut, TFileDeleteEvent SignalProc);
	__property Terminated;
	
__published:
	__property TFileDeleteEvent OnSignalDelete = {read=fOnSignalDelete, write=fOnSignalDelete};
public:
	/* TCompThread.Destroy */ inline __fastcall virtual ~TFileDeleteThread(void) { }
	
};


//-- var, const, procedure ---------------------------------------------------

}	/* namespace Filechanges */
using namespace Filechanges;
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// FilechangesHPP
