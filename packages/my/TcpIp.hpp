// CodeGear C++Builder
// Copyright (c) 1995, 2009 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'Tcpip.pas' rev: 21.00

#ifndef TcpipHPP
#define TcpipHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member functions
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <Sysinit.hpp>	// Pascal unit
#include <Sysutils.hpp>	// Pascal unit
#include <Classes.hpp>	// Pascal unit
#include <Winsock.hpp>	// Pascal unit
#include <Windows.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Tcpip
{
//-- type declarations -------------------------------------------------------
#pragma option push -b-
enum TSocketState { invalid, valid, connected, state_unknown };
#pragma option pop

#pragma option push -b-
enum TTraceLevel { tt_proto_sent, tt_proto_get, tt_socket };
#pragma option pop

class DELPHICLASS ETcpIpError;
class PASCALIMPLEMENTATION ETcpIpError : public Sysutils::Exception
{
	typedef Sysutils::Exception inherited;
	
public:
	/* Exception.Create */ inline __fastcall ETcpIpError(const System::UnicodeString Msg) : Sysutils::Exception(Msg) { }
	/* Exception.CreateFmt */ inline __fastcall ETcpIpError(const System::UnicodeString Msg, System::TVarRec const *Args, const int Args_Size) : Sysutils::Exception(Msg, Args, Args_Size) { }
	/* Exception.CreateRes */ inline __fastcall ETcpIpError(int Ident)/* overload */ : Sysutils::Exception(Ident) { }
	/* Exception.CreateResFmt */ inline __fastcall ETcpIpError(int Ident, System::TVarRec const *Args, const int Args_Size)/* overload */ : Sysutils::Exception(Ident, Args, Args_Size) { }
	/* Exception.CreateHelp */ inline __fastcall ETcpIpError(const System::UnicodeString Msg, int AHelpContext) : Sysutils::Exception(Msg, AHelpContext) { }
	/* Exception.CreateFmtHelp */ inline __fastcall ETcpIpError(const System::UnicodeString Msg, System::TVarRec const *Args, const int Args_Size, int AHelpContext) : Sysutils::Exception(Msg, Args, Args_Size, AHelpContext) { }
	/* Exception.CreateResHelp */ inline __fastcall ETcpIpError(int Ident, int AHelpContext)/* overload */ : Sysutils::Exception(Ident, AHelpContext) { }
	/* Exception.CreateResFmtHelp */ inline __fastcall ETcpIpError(System::PResStringRec ResStringRec, System::TVarRec const *Args, const int Args_Size, int AHelpContext)/* overload */ : Sysutils::Exception(ResStringRec, Args, Args_Size, AHelpContext) { }
	/* Exception.Destroy */ inline __fastcall virtual ~ETcpIpError(void) { }
	
};


class DELPHICLASS ESocketError;
class PASCALIMPLEMENTATION ESocketError : public ETcpIpError
{
	typedef ETcpIpError inherited;
	
public:
	unsigned ErrorNumber;
	__fastcall ESocketError(unsigned Number);
public:
	/* Exception.CreateFmt */ inline __fastcall ESocketError(const System::UnicodeString Msg, System::TVarRec const *Args, const int Args_Size) : ETcpIpError(Msg, Args, Args_Size) { }
	/* Exception.CreateRes */ inline __fastcall ESocketError(int Ident)/* overload */ : ETcpIpError(Ident) { }
	/* Exception.CreateResFmt */ inline __fastcall ESocketError(int Ident, System::TVarRec const *Args, const int Args_Size)/* overload */ : ETcpIpError(Ident, Args, Args_Size) { }
	/* Exception.CreateHelp */ inline __fastcall ESocketError(const System::UnicodeString Msg, int AHelpContext) : ETcpIpError(Msg, AHelpContext) { }
	/* Exception.CreateFmtHelp */ inline __fastcall ESocketError(const System::UnicodeString Msg, System::TVarRec const *Args, const int Args_Size, int AHelpContext) : ETcpIpError(Msg, Args, Args_Size, AHelpContext) { }
	/* Exception.CreateResHelp */ inline __fastcall ESocketError(int Ident, int AHelpContext)/* overload */ : ETcpIpError(Ident, AHelpContext) { }
	/* Exception.CreateResFmtHelp */ inline __fastcall ESocketError(System::PResStringRec ResStringRec, System::TVarRec const *Args, const int Args_Size, int AHelpContext)/* overload */ : ETcpIpError(ResStringRec, Args, Args_Size, AHelpContext) { }
	/* Exception.Destroy */ inline __fastcall virtual ~ESocketError(void) { }
	
};


class DELPHICLASS EProtocolError;
class PASCALIMPLEMENTATION EProtocolError : public ETcpIpError
{
	typedef ETcpIpError inherited;
	
public:
	System::Word ErrorNumber;
	System::UnicodeString Protocol;
	__fastcall EProtocolError(const System::UnicodeString Proto, const System::UnicodeString Msg, System::Word number);
public:
	/* Exception.CreateFmt */ inline __fastcall EProtocolError(const System::UnicodeString Msg, System::TVarRec const *Args, const int Args_Size) : ETcpIpError(Msg, Args, Args_Size) { }
	/* Exception.CreateRes */ inline __fastcall EProtocolError(int Ident)/* overload */ : ETcpIpError(Ident) { }
	/* Exception.CreateResFmt */ inline __fastcall EProtocolError(int Ident, System::TVarRec const *Args, const int Args_Size)/* overload */ : ETcpIpError(Ident, Args, Args_Size) { }
	/* Exception.CreateHelp */ inline __fastcall EProtocolError(const System::UnicodeString Msg, int AHelpContext) : ETcpIpError(Msg, AHelpContext) { }
	/* Exception.CreateFmtHelp */ inline __fastcall EProtocolError(const System::UnicodeString Msg, System::TVarRec const *Args, const int Args_Size, int AHelpContext) : ETcpIpError(Msg, Args, Args_Size, AHelpContext) { }
	/* Exception.CreateResHelp */ inline __fastcall EProtocolError(int Ident, int AHelpContext)/* overload */ : ETcpIpError(Ident, AHelpContext) { }
	/* Exception.CreateResFmtHelp */ inline __fastcall EProtocolError(System::PResStringRec ResStringRec, System::TVarRec const *Args, const int Args_Size, int AHelpContext)/* overload */ : ETcpIpError(ResStringRec, Args, Args_Size, AHelpContext) { }
	/* Exception.Destroy */ inline __fastcall virtual ~EProtocolError(void) { }
	
};


typedef void __fastcall (__closure *TTraceProc)(const System::UnicodeString S, TTraceLevel Level);

class DELPHICLASS TTcpIp;
class PASCALIMPLEMENTATION TTcpIp : public Classes::TComponent
{
	typedef Classes::TComponent inherited;
	
protected:
	int FSocket;
	System::UnicodeString FHostname;
	TTraceProc FTracer;
	short FSocketNumber;
	int IpAddress;
	bool FEof;
	Classes::TStream* FStream;
	void *FBuffer;
	bool FLoggedIn;
	int __fastcall CreateSocket(void);
	void __fastcall ConnectSocket(int &Socket, short SocketNumber, int IpAddress);
	void __fastcall BindSocket(int &Socket, System::Word OutPortMin, System::Word OutPortMax);
	virtual void __fastcall OpenSocketOut(int &Socket, short SocketNumber, int IpAddress);
	void __fastcall OpenSocketIn(int &Socket, short &SocketNumber, int IpAddress);
	void __fastcall CloseSocket(int &Socket);
	int __fastcall AcceptSocketIn(int Socket, sockaddr_in &SockInfo);
	TSocketState __fastcall SocketState(int Socket);
	short __fastcall SocketByName(const System::UnicodeString Service);
	System::UnicodeString __fastcall ReadLine(int Socket);
	void __fastcall ReadVar(int Socket, void *Buf, int Size, int &Ok);
	void __fastcall WriteBuf(int Socket, const void *Buf, int &Size);
	void __fastcall WriteStr(int Socket, const System::UnicodeString S);
	void __fastcall SetStream(Classes::TStream* Value);
	virtual void __fastcall Action(void);
	virtual void __fastcall SendCommand(const System::UnicodeString S);
	
public:
	virtual void __fastcall Login(void);
	virtual void __fastcall Logout(void);
	__property TTraceProc OnTrace = {read=FTracer, write=FTracer};
	__fastcall virtual TTcpIp(Classes::TComponent* AOwner);
	__fastcall virtual ~TTcpIp(void);
	virtual bool __fastcall Eof(int Socket);
};


class DELPHICLASS THttp;
class PASCALIMPLEMENTATION THttp : public TTcpIp
{
	typedef TTcpIp inherited;
	
protected:
	System::UnicodeString FUrl;
	System::UnicodeString FPath;
	System::UnicodeString FProxy;
	System::UnicodeString FSender;
	System::UnicodeString FReference;
	System::UnicodeString FAgent;
	bool FNoCache;
	int FStatusNr;
	System::UnicodeString FStatusTxt;
	int FSize;
	System::UnicodeString FType;
	Classes::TStringList* FDoAuthor;
	System::UnicodeString FContentPost;
	bool FRedirect;
	void __fastcall GetHead(void);
	void __fastcall GetBody(void);
	void __fastcall SendRequest(const System::UnicodeString Method, const System::UnicodeString Version);
	void __fastcall GetAnswer(void);
	void __fastcall ReportStatusError(void);
	
public:
	__property Classes::TStream* Stream = {read=FStream, write=SetStream};
	__property int ContentSize = {read=FSize, nodefault};
	__property System::UnicodeString ContentType = {read=FType};
	__property int StatusNumber = {read=FStatusNr, nodefault};
	__property System::UnicodeString StatusText = {read=FStatusTxt};
	virtual void __fastcall Action(void);
	void __fastcall Post(void);
	__fastcall virtual THttp(Classes::TComponent* AOwner);
	__fastcall virtual ~THttp(void);
	__property Classes::TStringList* AuthorizationRequest = {read=FDoAuthor};
	
__published:
	__property System::UnicodeString URL = {read=FUrl, write=FUrl};
	__property System::UnicodeString Proxy = {read=FProxy, write=FProxy};
	__property System::UnicodeString Sender = {read=FSender, write=FSender};
	__property System::UnicodeString Agent = {read=FAgent, write=FAgent};
	__property System::UnicodeString Reference = {read=FReference, write=FReference};
	__property bool NoCache = {read=FNoCache, write=FNoCache, nodefault};
	__property System::UnicodeString ContentTypePost = {read=FContentPost, write=FContentPost};
	__property OnTrace;
};


//-- var, const, procedure ---------------------------------------------------
extern PACKAGE System::ResourceString _SSocketError2;
#define Tcpip_SSocketError2 System::LoadResourceString(&Tcpip::_SSocketError2)
extern PACKAGE System::ResourceString _STimeout;
#define Tcpip_STimeout System::LoadResourceString(&Tcpip::_STimeout)
extern PACKAGE System::ResourceString _SUnknownSockError;
#define Tcpip_SUnknownSockError System::LoadResourceString(&Tcpip::_SUnknownSockError)
extern PACKAGE System::ResourceString _SHttpError;
#define Tcpip_SHttpError System::LoadResourceString(&Tcpip::_SHttpError)
extern PACKAGE System::ResourceString _SRedirectLimitError;
#define Tcpip_SRedirectLimitError System::LoadResourceString(&Tcpip::_SRedirectLimitError)
extern PACKAGE void __fastcall Register(void);

}	/* namespace Tcpip */
using namespace Tcpip;
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// TcpipHPP
