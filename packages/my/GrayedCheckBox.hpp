// CodeGear C++Builder
// Copyright (c) 1995, 2009 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'Grayedcheckbox.pas' rev: 21.00

#ifndef GrayedcheckboxHPP
#define GrayedcheckboxHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member functions
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <Sysinit.hpp>	// Pascal unit
#include <Stdctrls.hpp>	// Pascal unit
#include <Controls.hpp>	// Pascal unit
#include <Classes.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Grayedcheckbox
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TGrayedCheckBox;
class PASCALIMPLEMENTATION TGrayedCheckBox : public Stdctrls::TCheckBox
{
	typedef Stdctrls::TCheckBox inherited;
	
protected:
	virtual void __fastcall Toggle(void);
public:
	/* TCustomCheckBox.Create */ inline __fastcall virtual TGrayedCheckBox(Classes::TComponent* AOwner) : Stdctrls::TCheckBox(AOwner) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TGrayedCheckBox(HWND ParentWindow) : Stdctrls::TCheckBox(ParentWindow) { }
	/* TWinControl.Destroy */ inline __fastcall virtual ~TGrayedCheckBox(void) { }
	
};


//-- var, const, procedure ---------------------------------------------------
extern PACKAGE void __fastcall Register(void);

}	/* namespace Grayedcheckbox */
using namespace Grayedcheckbox;
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// GrayedcheckboxHPP
