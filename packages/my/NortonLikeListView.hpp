// CodeGear C++Builder
// Copyright (c) 1995, 2009 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'Nortonlikelistview.pas' rev: 21.00

#ifndef NortonlikelistviewHPP
#define NortonlikelistviewHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member functions
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <Sysinit.hpp>	// Pascal unit
#include <Windows.hpp>	// Pascal unit
#include <Messages.hpp>	// Pascal unit
#include <Sysutils.hpp>	// Pascal unit
#include <Classes.hpp>	// Pascal unit
#include <Graphics.hpp>	// Pascal unit
#include <Controls.hpp>	// Pascal unit
#include <Forms.hpp>	// Pascal unit
#include <Dialogs.hpp>	// Pascal unit
#include <Comctrls.hpp>	// Pascal unit
#include <Listviewcolproperties.hpp>	// Pascal unit
#include <Commctrl.hpp>	// Pascal unit
#include <Types.hpp>	// Pascal unit
#include <Imglist.hpp>	// Pascal unit
#include <Menus.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Nortonlikelistview
{
//-- type declarations -------------------------------------------------------
#pragma option push -b-
enum TSelectMode { smAll, smNone, smInvert };
#pragma option pop

#pragma option push -b-
enum TNortonLikeMode { nlOn, nlOff, nlKeyboard };
#pragma option pop

class DELPHICLASS TCustomNortonLikeListView;
typedef void __fastcall (__closure *TSelectByMaskEvent)(TCustomNortonLikeListView* Control, bool Select);

class PASCALIMPLEMENTATION TCustomNortonLikeListView : public Comctrls::TCustomListView
{
	typedef Comctrls::TCustomListView inherited;
	
private:
	Listviewcolproperties::TCustomListViewColProperties* FColProperties;
	bool FDontSelectItem;
	bool FDontUnSelectItem;
	int FSelCount;
	TNortonLikeMode FNortonLike;
	TSelectByMaskEvent FOnSelectByMask;
	Comctrls::TListItem* FLastDeletedItem;
	bool FFocusingItem;
	bool FManageSelection;
	bool FForceUpdateOnItemUnfocus;
	int FFirstSelected;
	int FLastSelected;
	System::TDateTime FFocused;
	unsigned FIgnoreSetFocusFrom;
	bool FSelectingImplicitly;
	bool FAnyAndAllSelectedImplicitly;
	Classes::TShiftState FLButtonDownShiftState;
	Types::TPoint FLButtonDownPos;
	System::Word FNextCharToIgnore;
	HIDESBASE MESSAGE void __fastcall WMLButtonDown(Messages::TWMMouse &Message);
	HIDESBASE MESSAGE void __fastcall WMRButtonDown(Messages::TWMMouse &Message);
	HIDESBASE MESSAGE void __fastcall WMLButtonUp(Messages::TWMMouse &Message);
	HIDESBASE MESSAGE void __fastcall WMKeyDown(Messages::TWMKey &Message);
	HIDESBASE MESSAGE void __fastcall WMChar(Messages::TWMKey &Message);
	HIDESBASE MESSAGE void __fastcall WMNotify(Messages::TWMNotify &Message);
	HIDESBASE MESSAGE void __fastcall CNNotify(Messages::TWMNotify &Message);
	MESSAGE void __fastcall LVMEditLabel(Messages::TMessage &Message);
	HIDESBASE MESSAGE void __fastcall WMSetFocus(Messages::TWMSetFocus &Message);
	MESSAGE void __fastcall CMWantSpecialKey(Messages::TWMKey &Message);
	int __fastcall GetMarkedCount(void);
	Comctrls::TListItem* __fastcall GetMarkedFile(void);
	void __fastcall ItemSelected(Comctrls::TListItem* Item, int Index);
	void __fastcall ItemUnselected(Comctrls::TListItem* Item, int Index);
	HIDESBASE void __fastcall SelectAll(TSelectMode Mode, Comctrls::TListItem* Exclude)/* overload */;
	
protected:
	bool FClearingItems;
	int FUpdatingSelection;
	virtual void __fastcall CreateWnd(void);
	virtual void __fastcall DestroyWnd(void);
	virtual void __fastcall BeginSelectionUpdate(void);
	virtual void __fastcall EndSelectionUpdate(void);
	virtual bool __fastcall CanChangeSelection(Comctrls::TListItem* Item, bool Select);
	virtual void __fastcall ClearItems(void);
	void __fastcall ItemsReordered(void);
	DYNAMIC void __fastcall ColRightClick(Comctrls::TListColumn* Column, const Types::TPoint &Point);
	DYNAMIC void __fastcall Delete(Comctrls::TListItem* Item);
	virtual bool __fastcall DoSelectByMask(bool Select);
	DYNAMIC bool __fastcall ExCanChange(Comctrls::TListItem* Item, int Change, System::Word NewState, System::Word OldState);
	DYNAMIC void __fastcall InsertItem(Comctrls::TListItem* Item);
	virtual Listviewcolproperties::TCustomListViewColProperties* __fastcall NewColProperties(void);
	virtual void __fastcall FocusSomething(void);
	virtual bool __fastcall EnableDragOnClick(void);
	void __fastcall FocusItem(Comctrls::TListItem* Item);
	Comctrls::TListItem* __fastcall GetItemFromHItem(const tagLVITEMW &Item);
	virtual bool __fastcall GetValid(void);
	virtual int __fastcall GetSelCount(void);
	void __fastcall DDBeforeDrag(void);
	DYNAMIC bool __fastcall CanEdit(Comctrls::TListItem* Item);
	
public:
	__fastcall virtual TCustomNortonLikeListView(Classes::TComponent* AOwner);
	__fastcall virtual ~TCustomNortonLikeListView(void);
	Comctrls::TListItem* __fastcall ClosestUnselected(Comctrls::TListItem* Item);
	HIDESBASE void __fastcall SelectAll(TSelectMode Mode)/* overload */;
	void __fastcall SelectCurrentItem(bool FocusNext);
	HIDESBASE Comctrls::TListItem* __fastcall GetNextItem(Comctrls::TListItem* StartItem, Comctrls::TSearchDirection Direction, Comctrls::TItemStates States);
	__property Listviewcolproperties::TCustomListViewColProperties* ColProperties = {read=FColProperties, write=FColProperties, stored=false};
	__property MultiSelect = {default=1};
	__property TNortonLikeMode NortonLike = {read=FNortonLike, write=FNortonLike, default=0};
	__property TSelectByMaskEvent OnSelectByMask = {read=FOnSelectByMask, write=FOnSelectByMask};
	__property int MarkedCount = {read=GetMarkedCount, nodefault};
	__property Comctrls::TListItem* MarkedFile = {read=GetMarkedFile};
	__property bool Valid = {read=GetValid, nodefault};
public:
	/* TWinControl.CreateParented */ inline __fastcall TCustomNortonLikeListView(HWND ParentWindow) : Comctrls::TCustomListView(ParentWindow) { }
	
};


class DELPHICLASS TNortonLikeListView;
class PASCALIMPLEMENTATION TNortonLikeListView : public TCustomNortonLikeListView
{
	typedef TCustomNortonLikeListView inherited;
	
__published:
	__property Align = {default=0};
	__property AllocBy = {default=0};
	__property Anchors = {default=3};
	__property BiDiMode;
	__property BorderStyle = {default=1};
	__property BorderWidth = {default=0};
	__property Checkboxes = {default=0};
	__property Color = {default=-16777211};
	__property ColumnClick = {default=1};
	__property Constraints;
	__property Ctl3D;
	__property Enabled = {default=1};
	__property Font;
	__property FlatScrollBars = {default=0};
	__property FullDrag = {default=0};
	__property GridLines = {default=0};
	__property HideSelection = {default=1};
	__property HotTrack = {default=0};
	__property HotTrackStyles = {default=0};
	__property IconOptions;
	__property Items;
	__property LargeImages;
	__property ReadOnly;
	__property RowSelect = {default=0};
	__property ParentBiDiMode = {default=1};
	__property ParentColor = {default=1};
	__property ParentFont = {default=1};
	__property ParentShowHint = {default=1};
	__property PopupMenu;
	__property ShowColumnHeaders = {default=1};
	__property ShowHint;
	__property SmallImages;
	__property StateImages;
	__property TabOrder = {default=-1};
	__property TabStop = {default=0};
	__property ViewStyle = {default=0};
	__property Visible = {default=1};
	__property OnChange;
	__property OnChanging;
	__property OnClick;
	__property OnColumnClick;
	__property OnCustomDraw;
	__property OwnerDraw = {default=0};
	__property OnCustomDrawItem;
	__property OnCustomDrawSubItem;
	__property OwnerData = {default=0};
	__property OnGetImageIndex;
	__property OnCompare;
	__property OnData;
	__property OnDataFind;
	__property OnDataHint;
	__property OnDataStateChange;
	__property OnDblClick;
	__property OnDeletion;
	__property OnDrawItem;
	__property OnEdited;
	__property OnEditing;
	__property OnEndDock;
	__property OnEnter;
	__property OnExit;
	__property OnInsert;
	__property OnKeyDown;
	__property OnKeyPress;
	__property OnKeyUp;
	__property OnMouseDown;
	__property OnMouseMove;
	__property OnMouseUp;
	__property OnResize;
	__property OnStartDock;
	__property OnSelectItem;
	__property NortonLike = {default=0};
	__property OnSelectByMask;
	__property ColProperties;
public:
	/* TCustomNortonLikeListView.Create */ inline __fastcall virtual TNortonLikeListView(Classes::TComponent* AOwner) : TCustomNortonLikeListView(AOwner) { }
	/* TCustomNortonLikeListView.Destroy */ inline __fastcall virtual ~TNortonLikeListView(void) { }
	
public:
	/* TWinControl.CreateParented */ inline __fastcall TNortonLikeListView(HWND ParentWindow) : TCustomNortonLikeListView(ParentWindow) { }
	
};


//-- var, const, procedure ---------------------------------------------------
extern PACKAGE void __fastcall Register(void);

}	/* namespace Nortonlikelistview */
using namespace Nortonlikelistview;
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// NortonlikelistviewHPP
