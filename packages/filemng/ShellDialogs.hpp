// CodeGear C++Builder
// Copyright (c) 1995, 2009 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'Shelldialogs.pas' rev: 21.00

#ifndef ShelldialogsHPP
#define ShelldialogsHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member functions
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <Sysinit.hpp>	// Pascal unit
#include <Windows.hpp>	// Pascal unit
#include <Shlobj.hpp>	// Pascal unit
#include <Sysutils.hpp>	// Pascal unit
#include <Classes.hpp>	// Pascal unit
#include <Messages.hpp>	// Pascal unit
#include <Menus.hpp>	// Pascal unit
#include <Pidl.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Shelldialogs
{
//-- type declarations -------------------------------------------------------
typedef StaticArray<Shlobj::PItemIDList, 1> TPIDLArray;

typedef TPIDLArray *PPIDLArray;

//-- var, const, procedure ---------------------------------------------------
#define shcProperties L"properties"
#define shcCut L"cut"
#define shcCopy L"copy"
#define shcPaste L"paste"
#define shcDelete L"delete"
#define shcLink L"link"
#define shcrename L"rename"
#define shcDefault L""
extern PACKAGE Menus::TPopupMenu* CustomContextMenu;
extern PACKAGE void __fastcall ShellDisplayContextMenu(unsigned Handle, const Types::TPoint &P, _di_IShellFolder ShellFolder, int PIDLCount, Shlobj::PItemIDList &PIDL, bool AllowRename, System::UnicodeString &Verb, bool PerformPaste = true)/* overload */;
extern PACKAGE void __fastcall ShellDisplayContextMenu(unsigned Handle, const Types::TPoint &P, System::UnicodeString FileName, bool AllowRename, System::UnicodeString &Verb, bool PerformPaste = true)/* overload */;
extern PACKAGE void __fastcall ShellDisplayContextMenu(unsigned Handle, const Types::TPoint &P, System::UnicodeString Path, Classes::TStringList* Files, System::UnicodeString &Verb, bool PerformPaste = true)/* overload */;
extern PACKAGE bool __fastcall ShellExecuteContextCommand(unsigned Handle, System::UnicodeString Command, _di_IShellFolder ShellFolder, int PIDLCount, Shlobj::PItemIDList &PIDL)/* overload */;
extern PACKAGE bool __fastcall ShellExecuteContextCommand(unsigned Handle, System::UnicodeString Command, System::UnicodeString FileName)/* overload */;
extern PACKAGE bool __fastcall ShellExecuteContextCommand(unsigned Handle, System::UnicodeString Command, System::UnicodeString Path, Classes::TStringList* Files)/* overload */;

}	/* namespace Shelldialogs */
using namespace Shelldialogs;
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// ShelldialogsHPP
